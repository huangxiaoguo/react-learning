let hName = "huangxiaoguo";

hName = 'xiaoguo';

console.log(hName)

const username = "huangxiaoguo"


//常量不可以修改（ts会报错）
// username ='xiaoguo'

/*******************************数据类型***************************/
let age: number = 19
// let age = 19

//number类型不能接收String类型（ts会报错）
// age = 'dhakj'


let city: string = 'huang'

let isHas: boolean = false

let sex: any //任意类型

sex = '男'

sex = 1

console.log(sex)


/*******************************函数***************************/

function echo() {
    console.log('hello')
}
echo()


function add(num1: number, num2: number) {
    return num1 + num2
}

function add2(num1: number, num2: number): string {
    return num1 + num2 + ''
}

console.log(add(1, 2))

// （ts会报错）
// console.log(add('1' , 2))


const minus = (num1: number, num2: number) => {
    return num2 - num1;
}

console.log(minus(1, 8))


export {};