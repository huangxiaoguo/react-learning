var hName = "huangxiaoguo";
hName = 'xiaoguo';
console.log(hName);
var username = "huangxiaoguo";
//常量不可以修改（ts会报错）
// username ='xiaoguo'
/*******************************数据类型***************************/
var age = 19;
// let age = 19
//number类型不能接收String类型（ts会报错）
// age = 'dhakj'
var city = 'huang';
var isHas = false;
var sex; //任意类型
sex = '男';
sex = 1;
console.log(sex);
/*******************************函数***************************/
function echo() {
    console.log('hello');
}
echo();
function add(num1, num2) {
    return num1 + num2;
}
function add2(num1, num2) {
    return num1 + num2 + '';
}
console.log(add(1, 2));
// （ts会报错）
// console.log(add('1' , 2))
var minus = function (num1, num2) {
    return num2 - num1;
};
console.log(minus(1, 8));
export {};
//# sourceMappingURL=01.%E5%9F%BA%E6%9C%AC%E7%B1%BB%E5%9E%8B.js.map