/*******************************联合类型**************************************** */
let money: string | number | boolean = 9;

money = "5";

money = 6;

console.log(money);
/*******************************数组类型**************************************** */
let languages: string[] = ["ruby", "java"];

languages.push("dart");
console.log(languages);

let nums: number[] = [1, 2];

nums.push(3);
console.log(nums);

//注意这里必须赋值[]不然运行时候“Cannot read property 'push' of undefined”
let n: Array<number> = [];

n.push(1);
console.log(n);

/*******************************对象类型*****************************************/
let o: object;

o = {
  name: "ruby",
  age: 18,
};

o = {
  name: "java",
  tags: ["python"],
};

o = {
  tags: ["python"],
};

o = [1];

// o = 1;

// o = "ruby";

console.log(o);

/*******************************元组*****************************************/

//确认每个item的类型，并且一一对应，可以使用数组的方法
let user: [string, number, string] = ["dadas", 1212, "aaaaaaaaaa"];

console.log(user); //[ 'dadas', 1212, 'aaaaaaaaaa' ]

//只能添加声明的类型[string, number, string]
user.push(11);
user.push('dasd');
console.log(user);//[ 'dadas', 1212, 'aaaaaaaaaa', 11, 'dasd' ]


export {};
