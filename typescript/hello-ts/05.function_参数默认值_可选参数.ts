
/*****************************参数默认值********************************* */
const add: Function = (num1: number, num2: number = 10) => {
    return num1 + num2;
};

function add2(num1: number, num2: number = 10) {
    return num1 + num2;
};

function add3(num1: number, num2: number = 10): number {
    return num1 + num2;
};

console.log(add(8))
console.log(add2(18))
const result = add(10, 50);
console.log(result);

/*****************************可选参数********************************* */
const minus: Function = (num1: number, num2?: number) => {
    if (num2) {
        return num1 - num2;
    }
    return num1;
};


const r = minus(5);

console.log(r);
console.log(minus(5, 9));

/*****************************传递函数********************************* */
function e(f: Function, num1: number, num2: number) {
    console.log(f(num1, num2));
}

e(add, 1, 2);

export { };