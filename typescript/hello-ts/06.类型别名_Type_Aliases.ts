
//将sting和number类型封装为StringOrNum类型
type StringOrNum = string | number;


const logDetails = (uid: StringOrNum, item: string) => {
  console.log(`${item} has a uid of ${uid}`);
};

logDetails(1111, "world");

// ------------------------------------------------------------------

//将  { name: string; uid: StringOrNum }封装为 objWithName类型

type objWithName = { name: string; uid: StringOrNum };

const greet = (user: objWithName) => {
  console.log(`${user.name} says hello ${user.uid}`);
};



greet({ name: "hello", uid: "1111" });

export {};