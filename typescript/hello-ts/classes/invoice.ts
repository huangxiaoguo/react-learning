class Invoice {
    // private client: string;
    // public details: string;
    // readonly amount: number;

    constructor(
        private client: string, //内部访问可修改
        public details: string,
        readonly amount: number //内部访问不可修改
    ) { }//简化的赋值写法

    public format() {
        this.client = this.client + '修改'
        //不可修改,只读
        // this.amount = this.amount + 1
        return `${this.client} owes ¥${this.amount} for ${this.details}`;
    }
}

export { Invoice };