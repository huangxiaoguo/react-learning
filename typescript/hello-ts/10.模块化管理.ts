import { Invoice } from "./classes/invoice";

const invOne = new Invoice("mario", "work on the mario website", 250);
const invTwo = new Invoice("luigi", "work on the luigi website", 300);

console.log(invOne);
console.log(invTwo);

//   invOne.client = "hfpp2012";
//
// console.log(invOne.amount);
//
//   invOne.amount = 90;
//
// console.log(invOne.client);

console.log(invOne.format());
console.log(invTwo.format());

let invoices: Invoice[] = [];

invoices.push(invOne);
invoices.push(invTwo);

console.log(invoices);

export { };