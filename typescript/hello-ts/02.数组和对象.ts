
/****************************数组***********************************/

let languages = ["ruby", "js", "dart", "python"];

console.log(languages[0]);

languages.push("java");

//ts报错
// languages.push(222);

languages[0] = "rust";

console.log(languages);



let staffs = ["ruby", 1, false];

staffs.push(1);

staffs.push("java");


/****************************对象***********************************/

let o = {
  name: "ruby",
  age: 19,
  color: "yellow",
};

console.log(o);

console.log(o["name"]);

o = {
  name: "java",
  age: 29,
  color: "red",
};

o["name"] = "python";

o.age = 80;

//ts报错
// o = {
//   name: "dart",
// };

console.log(o);
export { };