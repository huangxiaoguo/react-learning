
const addUID = <T extends { name: string }>(obj: T) => {
    let uid = Math.floor(Math.random() * 100);
    return { ...obj, uid };
};

let docOne = addUID({ age: 90, name: "hello" });

// -----------------------------------------------------------
interface Resource<T> {
    uid: number;
    resouceName: string;
    data: T;
    speak(): void;
}

const docTwo: Resource<string> = {
    uid: 1,
    resouceName: "person",
    data: "hello",
    speak(): void {
        console.log(`uid: ${this.uid} resouceName: ${this.resouceName}  data: ${this.data}`);
    },
};

const docThree: Resource<any> = {
    uid: 2,
    resouceName: "person",
    data: { name: "hello123444" },
    speak(): void {
        console.log(`uid: ${this.uid} resouceName: ${this.resouceName}  data: ${this.data.name}`);
    },
};

console.log(docTwo.speak())

console.log(docThree.speak())

export { };