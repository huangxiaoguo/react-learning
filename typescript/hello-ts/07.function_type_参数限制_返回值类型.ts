// let greet: Function;
//
// greet = (name: string): void => {
//   console.log(`hello ${name}`);
// };
//
// greet('world')

/**********************参数限制********************************** */
//声明
let greet: (a: string, b: string) => void;

//实现
greet = (name: string, greeting: string): void => {
  console.log(`${name} says ${greeting}`);
};

greet("hfpp2012", "hello");

/**********************参数限制，返回值限制********************************** */
let calc: (a: number, b: number, c: string) => number;

calc = (numOne: number, numTwo: number, action: string) => {
  if (action === "add") {
    return numOne + numTwo;
  } else {
    return numOne - numTwo;
  }
};

console.log(calc(5, 4, "add"));

/**********************参数限制，返回值限制,对象类型（别名）********************************** */

type person = { name: string; age: number };

let logDetails: (obj: person) => void;

logDetails = (obj: person) => {
  console.log(`${obj.name} is ${obj.age}`);
};

logDetails({ name: 'huangxiaoguo', age: 18 })

export { };