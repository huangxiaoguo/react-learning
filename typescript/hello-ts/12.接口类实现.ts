import { Invoice2 } from "./classes/invoice2";
import { Payment } from "./classes/payment";
import { HasFormatter } from "./interfaces/hasformatter";

let docOne: HasFormatter;
let docTwo: HasFormatter;

docOne = new Invoice2("mario", "web work", 250);
docTwo = new Payment("luigi", "plumbing work", 300);

console.log(docOne.format());
console.log(docTwo.format());


export {};