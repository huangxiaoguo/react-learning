export interface HasFormatter {
    name?: string;//可以实现也可以不实现
    age?: number;
    format(): string;
}