
interface IsPerson {
    name: string;
    age: number;
    speak(a: string): void;
    spend(a: number): string;
}

/***********************************普通实现**************************************** */

const me: IsPerson = {
    name: "shaun",
    age: 30,
    speak(text: string): void {
        console.log(text);
    },
    spend(amount: number): string {
        console.log("I spent", amount);
        return amount + '';
    },
};

const greetPerson = (person: IsPerson) => {
    console.log("hello " + person.name);
};

console.log(me);

greetPerson(me);



/***********************************类实现**************************************** */

console.log('*************************类实现*************************');
class Xiaoming implements IsPerson {
    name = "shaun";
    age = 30;

    constructor(name: string, age: number) {
        this.name = name;
        this.age = age;
    }

    speak(text: string): void {
        console.log(text);
    }
    spend(amount: number): string {
        console.log("I spent", amount);
        return `name: ${this.name} age: ${this.age}  amount: ${amount}`;
    }
};

const xiao = new Xiaoming('huang', 18)

xiao.speak('woaini123')

console.log(xiao.spend(82))