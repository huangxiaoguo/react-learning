// classes

class Invoice {
    // private client: string;
    // public details: string;
    // readonly amount: number;

    constructor(
        private client: string, //内部访问可修改
        public details: string,
        readonly amount: number //内部访问不可修改
    ) { }//简化的赋值写法

    public format() {
        this.client = this.client + '修改'
        //不可修改,只读
        // this.amount = this.amount + 1
        return `${this.client} owes ¥${this.amount} for ${this.details}`;
    }
}

const invOne = new Invoice("mario", "work on the mario website", 250);
const invTwo = new Invoice("luigi", "work on the luigi website", 300);

console.log(invOne);
console.log(invTwo);

//   invOne.client = "hfpp2012";
//
// console.log(invOne.amount);
//
//   invOne.amount = 90;
//
// console.log(invOne.client);

console.log(invOne.format());
console.log(invTwo.format());

let invoices: Invoice[] = [];

invoices.push(invOne);
invoices.push(invTwo);

console.log(invoices);

export { };