import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { path: '/', component: '@/pages/users/index' },
  ],
  fastRefresh: {},
  proxy: {
    '/api': {
      'target': 'https://www.fastmock.site/mock/fed13828390069db6c28fe3f2da98a19/demo1/',
      // 'target': 'https://public-api-v1.aspirantzhang.com/',
      'changeOrigin': true,
      'pathRewrite': { '^/api': '' },
    },
  },
});
