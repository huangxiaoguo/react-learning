import React, { useState, FC } from 'react';

//UserState, SingleUserType 需要在model中导出
import { connect, Dispatch, Loading, UserState, useModel } from 'umi';

import { Table, Space } from 'antd';
import UserModal from './components/UserModal'
import { SingleUserType, FormValues } from './data'

/**
 * 声明下方 props 类型
 * const { users, dispatch, listLoading } = props
 */
type userPageProps = {
    users: UserState,
    dispatch: Dispatch,
    listLoading: boolean
}

const UserListPage: FC<userPageProps> = (props) => {
    //控制modal弹框
    const [visible, setVisible] = useState(false);
    const [confirmLoading, setConfirmLoading] = useState(false);
    const [record, setRecord] = useState<SingleUserType | undefined>(undefined);


    //使用全局models 

    const { user, setUserData } = useModel('appstore')

    //获取从model中来的数据    
    const { users, dispatch, listLoading } = props

    //编辑
    const onClickEdit = (record: SingleUserType) => {
        setVisible(true)
        setRecord(record)

        /**
         * 修改全局参数
         */
        console.log(user)
        setUserData('huang', '123456')
    };
    /**
     * 表单提交
     */
    const onFinish = (value: FormValues) => {
        setConfirmLoading(true);
        const id = record?.id
        //usersData为model的命名空间
        dispatch({
            type: 'usersData/editItem',
            payload: {
                id,
                value
            }
        })
        //此处并不能保存usersData/editItem接口先执行
        setVisible(false);
        setConfirmLoading(false);

        /**
       * 查看全局参数
       */
        console.log(user)
    };

    //关闭弹窗
    const handleCancel = () => {
        setVisible(false);
    };
    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
        },
        {
            title: 'Name',
            dataIndex: 'name',
            render: (text: string) => <a>{text}</a>,
        },
        {
            title: 'CreateTime',
            dataIndex: 'create_time',
        },
        {
            title: 'Action',
            render: (text: string, record: SingleUserType) => (
                <Space size="middle">
                    <a onClick={() => onClickEdit(record)}>编辑</a>
                    <a>删除</a>
                </Space>
            ),
        },
    ];


    return (
        <div className="list-table">
            <Table columns={columns} dataSource={users.data} rowKey={columns => columns.id} loading={listLoading} />
            <UserModal visible={visible} confirmLoading={confirmLoading}
                onFinish={onFinish} //关联子组件方法
                handleCancel={handleCancel}
                record={record} />
        </div>
    )
}
/**
 * 这是从model中获取数据
 * @param param0 
 * @returns 
 */
const mapStateToProps = ({ usersData, loading }: { usersData: UserState, loading: Loading }) => {
    return {
        users: usersData,//这里的usersData就是model中的namespace
        listLoading: loading.models.usersData
    }
}
/**
 *  mapStateToProps 简写
 */
/*
const mapStateToProps = ({ users }) => ({
    users
})
*/

export default connect(mapStateToProps)(UserListPage)

//最终简写
// export default connect(({ users }) => ({
//     users
// }))(UserListPage)