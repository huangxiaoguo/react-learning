import { Effect, Reducer, Subscription } from 'umi';

//导入service远端数据请求
import { getRemoteList, editRecord } from './service'

import { SingleUserType } from './data'

/**
 * 封装后台返回的数据
 */
export type UserState = {
    data: SingleUserType[],
    meta: {
        total: number,
        per_page: number,
        page: number
    }

}

interface UserModelType {
    namespace: 'usersData';//这里的命名空间就是展示页面得到数据的键
    state: UserState;//封装后台返回的数据
    effects: {
        getRemote: Effect;
        editItem: Effect;
    };
    reducers: {
        getList: Reducer<UserState>,
    };
    subscriptions: { setup: Subscription };
}

const UserModel: UserModelType = {
    namespace: 'usersData',//这里的命名空间就是展示页面得到数据的键

    state: {
        data: [],
        meta: {
            total: 0,
            per_page: 10,
            page: 1
        }
    },
    //异步
    effects: {
        //获取列表数据
        *getRemote({ payload }, { call, put }) {
            //从service中获取数据（service主要用于接口请求）
            const data = yield call(getRemoteList)
            yield put({
                type: 'getList',//这个是将数据给reducers中哪个方法
                payload: data  //注意这里传递一个数组会出问题，原因待定
            })
        },
        //修改列表数据
        *editItem({ payload: { id, value } }, { call, put }) {
            const data = yield call(editRecord, { id, value })
            /**
             * 调用列表页面
             */
            yield put({
                type: 'getRemote',//这个是将数据给reducers中哪个方法
            })
        },
    },
    //同步
    reducers: {
        getList(state, action) {
            return action.payload;
        },
    },
    //订阅
    subscriptions: {
        setup({ dispatch, history }) {
            return history.listen(({ pathname }) => {
                if (pathname === '/') {//当前页面的路径
                    dispatch({
                        type: 'getRemote',//调用effects中的方法
                    })
                }
            });
        }
    }
};

export default UserModel;