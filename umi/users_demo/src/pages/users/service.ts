
import { request as request1 } from 'umi';
import { message } from 'antd';

import request from '@/utils/request';

import { FormValues } from './data'

export async function getRemoteList1() {
    return request1('/api/users', {
        method: 'get'
    })
        .then(response => {
            return response
        })
        .catch(error => {
            console.log(error);
        });
}

export async function editRecord1({ id, value }: any) {
    return request1(`/api/edit`, {
        method: 'post',
        data: value
    })
        .then(response => {
            if (response.code != 200) {
                message.error(response.message);
            }
            return response
        })
        .catch(error => {
            message.error(error);
        });
}

// -----------------------------------使用封装后的request---------------------------------------------

type EditRecordParamsType = {
    id: number;
    value: FormValues;
};


export async function getRemoteList(): Promise<any> {
    return request('/api/users');
}

export async function editRecord(params: EditRecordParamsType): Promise<any> {
    return request('/api/edit', {
        method: 'POST',
        data: params,
    });
}
