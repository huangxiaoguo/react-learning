import { useState, useCallback } from 'react'
export default () => {
    const [user, setUser] = useState({})

    const setUserData = useCallback((account, password) => {
        setUser({ account: account, password: password })
    }, [])

    return { user, setUserData }
}