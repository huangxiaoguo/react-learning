import React, { useState, useRef, FC } from 'react';

import { PlusOutlined } from '@ant-design/icons';
import { Space, message, Button } from 'antd';
import ProTable from '@ant-design/pro-table';

import UserModal from './components/UserModal'
import { SingleUserType, FormValues } from './data'
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import { editRecord, getRemoteList } from './service'

type ListParamsType = {
    pageSize: number,
    current: number,
    id?: string,
    startTime?: string,
    endTime?: string
}

const UserListPage: FC<any> = () => {
    //控制modal弹框
    const [visible, setVisible] = useState(false);
    const [confirmLoading, setConfirmLoading] = useState(false);
    const [record, setRecord] = useState<SingleUserType | undefined>(undefined);

    //配置ProTable
    const ref = useRef<ActionType>();

    //编辑
    const onClickEdit = (record: SingleUserType) => {
        setVisible(true)
        setRecord(record)
    };
    /**
     * 表单提交
     */
    const onFinish = async (value: FormValues) => {
        setConfirmLoading(true);
        const id = record?.id
        const result = await editRecord({ id, value })
        if (result && result instanceof Object) {
            message.success(result.data);
            setVisible(false);
            // 刷新
            ref.current?.reload();
        }
        setConfirmLoading(false);
    };

    /**
     * ProTable的网络请求 request
     */
    const requestHandler = async (params: ListParamsType) => {
        console.log(params)
        const users = await getRemoteList(params);
        return {
            data: users.data,
            success: true,
            total: users.meta?.total
        }
    }

    //关闭弹窗
    const handleCancel = () => {
        setVisible(false);
    };

    const columns: ProColumns<SingleUserType>[] = [
        {
            title: 'ID',
            dataIndex: 'id',
            width: 68,
        },
        {
            title: '名称',
            dataIndex: 'name',
            search: false,
            ellipsis: true,
            render: (text) => <a>{text}</a>,
        },
        {
            title: '创建时间',
            key: 'showTime',
            dataIndex: 'create_time',
            valueType: 'dateTime',
            hideInSearch: true,
        },
        {
            title: '创建时间',
            dataIndex: 'create_time',
            valueType: 'dateTimeRange',
            hideInTable: true,
            colSize:2,
            search: {
                transform: (value) => {
                    console.log(value)
                    return {
                        startTime: value[0],
                        endTime: value[1],
                    };
                },
            },
        },
        {
            title: '操作',
            render: (text, record: SingleUserType) => (
                <Space size="middle">
                    <a onClick={() => onClickEdit(record)}>编辑</a>
                    <a>删除</a>
                </Space>
            ),
        },
    ];


    return (
        <div className="list-table">
            <ProTable request={requestHandler}
                actionRef={ref}
                columns={columns}
                rowKey="id"
                // search={false}
                search={{
                    labelWidth: 'auto',
                }}
                pagination={{
                    showQuickJumper: true,
                    pageSize: 10,
                }}
                form={{
                    // 由于配置了 transform，提交的参与与定义的不同这里需要转化一下
                    syncToUrl: (values, type) => {
                        if (type === 'get') {
                            return {
                                ...values,
                                create_time: [values.startTime, values.endTime],
                            };
                        }
                        return values;
                    },
                    span:4
                }}
                toolBarRender={() => [
                    <Button key="button" icon={<PlusOutlined />} type="primary">
                        新建
                    </Button>
                ]}
            />
            <UserModal visible={visible} confirmLoading={confirmLoading}
                onFinish={onFinish} //关联子组件方法
                handleCancel={handleCancel}
                record={record} />
        </div>
    )
}

export default UserListPage
