import React, { useEffect, FC } from 'react'
import { Modal, Form, Input, DatePicker ,Switch } from 'antd';
import moment from 'moment';

import { SingleUserType, FormValues } from '../data'

/**
 * 声明下方 props 类型
 * const { visible, confirmLoading, record, handleCancel, onFinish } = props
 */
type userModalProps = {
    visible: boolean,
    confirmLoading: boolean,
    record: SingleUserType | undefined,
    handleCancel: () => void,
    onFinish: (values: FormValues) => void
}

const UserModal: FC<userModalProps> = (props) => {

    const { visible, confirmLoading, record, handleCancel, onFinish } = props
    const [form] = Form.useForm();

    //相当于componentDidMount
    useEffect(() => {
        if (record == undefined) {
            form.resetFields()
        } else {
            //普通输入框
            // form.setFieldsValue(record);
            //处理DatePicker
            form.setFieldsValue({
                ...record,
                create_time:moment(record.create_time),
                status:record.status===1?true:false
            })
        }
    }, [visible])

    //点击弹窗
    const handleOk = () => {
        form.submit()
    };

    /**
     * 表单失败
     */
    const onFinishFailed = (err: any) => {
        console.log(err)
    };
    return (
        <div>
            <Modal
                title="Title"
                forceRender
                visible={visible}
                okText='确定'
                cancelText="取消"
                onOk={handleOk}//传递父组件处理
                confirmLoading={confirmLoading}
                onCancel={handleCancel}//传递父组件处理
            >
                <Form
                    form={form}
                    name="basic"
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        label="name"
                        name="name"
                        rules={[{ required: true, message: '请输入name' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="CreateTime"
                        name="create_time"
                        rules={[{ required: true, message: '请选择create_time' }]}
                    >
                        <DatePicker showTime />
                    </Form.Item>
                    <Form.Item
                        label="status"
                        name="status"
                        valuePropName="checked"
                        rules={[{ required: true, message: '请输入status' }]}
                    >
                        <Switch  />
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}

export default UserModal;
