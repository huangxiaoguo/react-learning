/**
 * 这个文件是放 公共的 类型接口的
 */

import { Key } from "react"

/**
 * 封装后台返回的数据
 */
export type SingleUserType ={
    id: number,
    name?: string,
    email?: string,
    create_time?: string,
    update_time?: string,
    status: number
}

/**
 * Modal 框的确定按钮的类型
 */
export type FormValues ={
    [name: string]: any
}