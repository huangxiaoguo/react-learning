﻿export default [
  {
    path: '/',
    component: '../layouts/BlankLayout',
    routes: [
      {
        path: '/user',
        component: '../layouts/UserLayout',
        routes: [
          {
            name: '登录',
            path: '/user/login',
            component: './User/login',
          },
          {
            component: './404',
          },
        ],
      },
      {
        path: '/',
        component: '../layouts/SecurityLayout',
        routes: [
          {
            path: '/',
            component: '../layouts/BasicLayout',
            routes: [
              {
                path: '/',
                redirect: '/welcome',
              },
              {
                path: '/welcome',
                component: './Welcome',
              },
              {
                path: '/Usermanage',
                icon: 'form',
                name: '用户管理',
                authority: ['admin'],
                routes: [
                  {
                    path: '/',
                    redirect: '/usermanage/rolemanage',
                  },
                  {
                    name: '角色管理',
                    icon: 'form',
                    path: '/usermanage/rolemanage',
                    component: './Usermanage/rolemanage',
                    authority: ['admin'],
                  },
                  {
                    name: '添加角色',
                    icon: 'form',
                    hideInMenu: true,
                    path: '/usermanage/rolemanage/addrole',
                    component: './Usermanage/rolemanage/addrole',
                  },
                  {
                    name: '账号列表',
                    icon: 'form',
                    path: '/usermanage/accountlist',
                    component: './Usermanage/accountlist',
                    authority: ['admin'],
                  },
                ],
              },
              {
                path: '/voicemanage',
                icon: 'form',
                name: '语音管理',
                authority: ['user'],
                routes: [
                  {
                    path: '/',
                    redirect: '/voicemanage/albumscategories',
                  },
                  {
                    name: '专辑分类',
                    icon: 'form',
                    path: '/voicemanage/albumscategories',
                    component: './voicemanage/albumscategories',
                  },
                  {
                    name: '收听管理',
                    icon: 'form',
                    path: '/voicemanage/listenmanage',
                    component: './voicemanage/listenmanage',
                  },
                ],
              },
              {
                path: '/appmanagement',
                icon: 'form',
                name: 'App管理',
                routes: [
                  {
                    path: '/',
                    redirect: '/appmanagement/banner',
                  },
                  {
                    name: 'Banner管理',
                    icon: 'form',
                    path: '/appmanagement/banner',
                    component: './appmanagement/banner',
                  }, {
                    name: '添加Banner',
                    icon: 'form',
                    hideInMenu: true,
                    path: '/appmanagement/banner/addbanner',
                    component: './appmanagement/banner/addbanner',
                  },
                  {
                    name: 'App版本管理',
                    icon: 'form',
                    path: '/appmanagement/appversions',
                    component: './appmanagement/appversions',
                  },
                ],
              },
              {
                path: '/vipmanagement',
                icon: 'form',
                name: 'VIP管理',
                authority: ['admin'],
                routes: [
                  {
                    path: '/',
                    redirect: '/vipmanagement/setmeal',
                  },
                  {
                    name: '套餐管理',
                    icon: 'form',
                    path: '/vipmanagement/setmeal',
                    component: './vipmanagement/setmeal',
                    authority: ['user'],
                  },
                  {
                    name: '激活码管理',
                    icon: 'form',
                    path: '/vipmanagement/activationcode',
                    component: './vipmanagement/activationcode',
                    authority: ['admin'],
                  },
                  {
                    name: '激活码生成列表',
                    icon: 'form',
                    path: '/vipmanagement/activationcodelist',
                    component: './vipmanagement/activationcodelist',
                    authority: ['admin'],
                  },
                  {
                    name: '激活码导出列表',
                    icon: 'form',
                    path: '/vipmanagement/activationcodeexport',
                    component: './vipmanagement/activationcodeexport',
                    authority: ['admin'],
                  },
                  {
                    name: '订单列表',
                    icon: 'form',
                    path: '/vipmanagement/orderlist',
                    component: './vipmanagement/orderlist',
                    authority: ['admin'],
                  },
                  {
                    name: 'VIP权益说明',
                    icon: 'form',
                    path: '/vipmanagement/vipequity',
                    component: './vipmanagement/vipequity',
                    authority: ['admin'],
                  },
                ],
              },
              {
                path: '/brandownermanage',
                icon: 'form',
                name: '品牌商管理',
                authority: ['user'],
                routes: [
                  {
                    path: '/',
                    redirect: '/brandownermanage/brandownerlist',
                  },
                  {
                    name: '品牌商列表',
                    icon: 'form',
                    path: '/brandownermanage/brandownerlist',
                    component: './brandownermanage/brandownerlist',
                    authority: ['admin'],
                  },
                ],
              },
              {
                path: '/machinemanage',
                icon: 'form',
                name: '机器管理', authority: ['user'],
                routes: [
                  {
                    path: '/',
                    redirect: '/machinemanage/machinelist',
                  },
                  {
                    name: '机器列表',
                    icon: 'form',
                    path: '/machinemanage/machinelist',
                    component: './machinemanage/machinelist',
                    authority: ['admin'],
                  },
                ],
              },
              {
                path: '/softwaremanage',
                icon: 'form',
                name: '软件管理',
                authority: ['user'],
                routes: [
                  {
                    path: '/',
                    redirect: '/softwaremanage/versions',
                  },
                  {
                    name: '版本管理',
                    icon: 'form',
                    path: '/softwaremanage/versions',
                    component: './softwaremanage/versions',
                    authority: ['admin'],
                  },
                  {
                    name: '使用说明',
                    icon: 'form',
                    path: '/softwaremanage/usedirection',
                    component: './softwaremanage/usedirection',
                    authority: ['admin'],
                  },
                ],
              },
              {
                path: '/officialaccounts',
                icon: 'form',
                name: '公众号设置',
                authority: ['user'],
                routes: [
                  {
                    path: '/',
                    redirect: '/officialaccounts/accesssettings',
                  },
                  {
                    name: '接入设置',
                    icon: 'form',
                    path: '/officialaccounts/accesssettings',
                    component: './officialaccounts/accesssettings',
                    authority: ['admin'],
                  },
                  {
                    name: '菜单设置',
                    icon: 'form',
                    path: '/officialaccounts/menu',
                    component: './officialaccounts/menu',
                    authority: ['admin'],
                  }, {
                    name: '关注回复',
                    icon: 'form',
                    path: '/officialaccounts/attentionreply',
                    component: './officialaccounts/attentionreply',
                    authority: ['admin'],
                  }, {
                    name: '关键字回复',
                    icon: 'form',
                    path: '/officialaccounts/keywordreply',
                    component: './officialaccounts/keywordreply',
                    authority: ['admin'],
                  }, {
                    name: '素材管理',
                    icon: 'form',
                    path: '/officialaccounts/material',
                    component: './officialaccounts/material',
                    authority: ['admin'],
                  },
                ],
              },
              {
                component: './404',
              },
            ],
          },
          {
            component: './404',
          },
        ],
      },
    ],
  },
  {
    component: './404',
  },
];
