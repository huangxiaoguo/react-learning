import React, { useState, useEffect } from 'react';
import { PageContainer, WaterMark } from '@ant-design/pro-layout';
import { Form, Card, Input, Tree, Button, message } from 'antd';
import '../index.less'
import { getRoleMenu, getRoleInfo, roleAdd } from '../service';
import { RoleMenuType } from '../data'
import { history } from 'umi';

export default (props: any): React.ReactNode => {
    const { id } = props.location?.state
    const [form] = Form.useForm();
    const [treeData, setTreeData] = useState<any>([]);
    const [checkedKeys, setCheckedKeys] = useState<React.Key[]>([]);

    const layout = {
        labelCol: { span: 4 },
        wrapperCol: { span: 6 },
    };
    const tailLayout = {
        wrapperCol: { offset: 4, span: 16 },
    };
    /**
     * 遍历权限数组
     * @param menuList 
     * @returns 
     */
    const treeListFun = (menuList: RoleMenuType[]): RoleMenuType[] => {
        menuList.forEach(item => {
            item.key = item.id + '';
            item.title = item.name;
            item.children = item.children ? treeListFun(item.children) : undefined;
        });
        return menuList
    }
    //相当于componentDidMount
    useEffect(() => {
        (async function onCreateFun() {
            const response = await getRoleMenu({ role_id: id })
            if (response && response instanceof Object) {
                const { data } = response
                setTreeData(treeListFun(data))
            }
            if (id) {
                const roleInfo = await getRoleInfo({ id: id })
                if (roleInfo && roleInfo instanceof Object) {
                    const { role_name, menu_ids } = roleInfo.data.info
                    setCheckedKeys(menu_ids.split(','))
                    form.setFieldsValue({
                        role_name: role_name
                    })
                }
            }

        })();
    }, [])


    const onFinish = async (values: any) => {
        if (Object.keys(checkedKeys).length == 0) {
            message.info("请选择权限")
            return
        }
        const menu_ids = checkedKeys.map(item => Number(item))
        const response = await roleAdd({ role_id: id, ...values, menu_ids: JSON.stringify(menu_ids) })
        if (response && response instanceof Object) {
            message.success(response.message)
            history.go(-1)
        }
    };

    const onCheck = (checkedKeys: any) => {
        setCheckedKeys(checkedKeys)
    };

    return (
        <PageContainer>
            <Card>
                <WaterMark content="御码科技" className='app-container'>
                    <Form
                        {...layout}
                        form={form}
                        name="basic"
                        onFinish={onFinish}
                    >
                        <Form.Item
                            label="名称"
                            name="role_name"
                            rules={[{ required: true, message: '请输入名称' }]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="权限"
                        >
                            <div className='tree-list-box'>
                                <Tree
                                    checkable
                                    checkedKeys={checkedKeys}
                                    onCheck={onCheck}
                                    treeData={treeData}
                                />
                            </div>
                        </Form.Item>
                        <Form.Item  {...tailLayout}>
                            <Button type="primary" htmlType="submit">
                                提交
                            </Button>
                        </Form.Item>
                    </Form>
                </WaterMark>
            </Card>
        </PageContainer>
    );
};
