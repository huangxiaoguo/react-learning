import request from '@/utils/request';
import type { ListParamsType } from '@/services/data'
import type { RoleAddType } from './data'
/**
 * 权限列表数据
 * @param params 
 * @returns 
 */
export async function getRoleList(params: ListParamsType): Promise<any> {
    return request('/Admin/Role/List', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}


/**
 * 停用、启用
 * @param params 
 * @returns 
 */
export async function roleFrozen(params: { id: number }): Promise<any> {
    return request('/Admin/Role/Frozen', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}


/**
 * 获取角色的权限列表(全部权限+选择)
 * @param params 
 * @returns 
 */
export async function getRoleMenu(params: { role_id: number }): Promise<any> {
    return request('/Admin/Role/GetRoleMenu', {
        method: 'GET',
        params: { ...params },
        requestType: 'form'
    });
}

/**
 * 獲取角色信息
 * @param params 
 * @returns 
 */
export async function getRoleInfo(params: { id: number }): Promise<any> {
    return request('/Admin/Role/Add', {
        method: 'GET',
        params: { ...params },
        requestType: 'form'
    });
}


/**
 * 角色 添加 编辑
 * @param params 
 * @returns 
 */
export async function roleAdd(params: RoleAddType): Promise<any> {
    return request('/Admin/Role/Add', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}
