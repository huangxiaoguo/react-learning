import React, { useRef, FC } from 'react';
import { PageContainer, WaterMark } from '@ant-design/pro-layout';
import { Card, Space, Button, message } from 'antd';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table'
import { PlusOutlined } from '@ant-design/icons';
import { SingleRoleListType } from './data'
import type { ListParamsType } from '@/services/data'
import { getRoleList, roleFrozen } from './service';
import './index.less'
import { Link } from 'umi';

const RoleManagePage: FC<any> = () => {
  //配置ProTable
  const ref = useRef<ActionType>();

  /**
   * ProTable的网络请求 request
   */
  const requestHandler = async (params: ListParamsType) => {
    const roleList = await getRoleList(params = { ...params, perpage: params.pageSize, page: params.current });
    if (roleList && roleList instanceof Object) {
      return {
        data: roleList.data.rows,
        success: true,
        total: roleList.data?.total
      }
    }
    return {
      success: false,
    }
  }
  /**
   * 启用或停用
   * @param id 
   */
  const onUseTypeClick = async (id: number) => {
    const response = await roleFrozen({ id: id });
    if (response && response instanceof Object) {
      message.success(response.message)
      ref.current?.reload();
    }
  }

  const columns: ProColumns<SingleRoleListType>[] = [
    {
      title: 'ID',
      dataIndex: 'id',
      width: 128,
      search: false,
      align: 'left'
    },
    {
      title: '角色名称',
      dataIndex: 'role_name',
      ellipsis: true,
      align: 'left'
    },
    {
      title: '状态',
      dataIndex: 'is_enable',
      search: false,
      align: 'left',
      render: (text) => <span>{text == 1 ? `启用` : `停用`}</span>,
    },
    {
      title: '创建时间',
      dataIndex: 'ctime_str',
      search: false,
      align: 'left'
    },
    {
      title: '操作',
      align: 'center',
      render: (text, record) => (
        <Space size="middle">
          <Link to={{ pathname: '/usermanage/rolemanage/addrole', state: { id: record.id } }}>编辑</Link>
          {
            record.is_enable == 0 ? <a className="start-using" onClick={() => onUseTypeClick(record.id)}>启用</a> :
              <a onClick={() => onUseTypeClick(record.id)} className="stop-using">停用</a>
          }
        </Space>
      ),
    },
  ];

  return (
    <PageContainer content="此模块可以配置子账号">
      <Card>
        <WaterMark content="御码科技">
          <ProTable
            request={requestHandler}
            columns={columns}
            actionRef={ref}
            rowKey="id"
            search={{
              labelWidth: 'auto',
            }}
            pagination={{
              showQuickJumper: true,
              pageSize: 10,
            }}
            form={{
              span: 6
            }}
            toolBarRender={() => [
              <Link to={{ pathname: '/usermanage/rolemanage/addrole', state: { id: 0 } }}>
                <Button key="button" icon={<PlusOutlined />} type="primary">
                  添加角色
                </Button>
              </Link>
            ]}
          />
        </WaterMark>
      </Card>
    </PageContainer>
  )
}
export default RoleManagePage