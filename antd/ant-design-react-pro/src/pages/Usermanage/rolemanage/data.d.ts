
/**
 * 封装后台返回的列表数据
 */
export type SingleRoleListType = {
    id: number,
    role_name?: string,
    menu_ids?: string,
    is_enable?: number,
    ctime?: number,
    uptime: number,
    ctime_str: string
}

/**
 * 封装后台返回的获取角色的权限列表(全部权限+选择)
 */
export type RoleMenuType = {
    id?: number,
    icon?: string,
    is_select?: number,
    pid?: number,
    url?: string,
    name?: string,
    title?: string,
    key?: string,
    children?: RoleMenuType[]
}


export type RoleAddType = {
    role_id: number,
    role_name?: string,
    menu_ids?: string
}