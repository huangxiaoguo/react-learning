
/**
 * 封装后台返回的数据
 */
export type SingleUserListType = {
    id: number,
    level?: number,
    account?: string,
    password?: string,
    contact_name?: string,
    contact_mobile?: string,
    remark?: string,
    role_id?: number,
    is_enable?: number,
    ctime?: string,
    uptime?: string,
    role_name?: string,
    ctime_str: string,
}

/**
 * 添加编辑账户
 */
export type UserAddType = {
    user_id?: number,
    account?: string,
    account_password?: string,
    contact_name?: string,
    contact_mobile?: number,
    role_id?: number,
};


/**
 * 獲取账户信息
 */
export type AccountInfoType = {
    user_id?: number,
    account?: string,
    password?: string,
    contact_name?: string,
    contact_mobile?: number,
    role_id?: number,
    id?: number
};

/**
 * 獲取账户信息列表
 */
export type AccountRoleListType = {
    ctime?: number,
    ctime_str?: string,
    id?: number,
    is_enable?: number,
    menu_ids?: string,
    role_name?: string,
    uptime?: number,
    value?:number,
    label?:string,
    disabled?:boolean
};