import request from '@/utils/request';
import type { ListParamsType } from '@/services/data'
import type { AccountInfoType } from './data'
/**
 * 列表数据
 * @param params 
 * @returns 
 */
export async function getUserList(params: ListParamsType): Promise<any> {
    return request('/Admin/User/List', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}

/**
 * 獲取账户信息
 * @param params 
 * @returns 
 */
export async function getUserInfo(params: { id: string }): Promise<any> {
    return request('/Admin/User/Add', {
        method: 'GET',
        params: { ...params },
        requestType: 'form'
    });
}
/**
 * 用户添加
 * @param params 
 * @returns 
 */
export async function userAdd(params: AccountInfoType): Promise<any> {
    return request('/Admin/User/Add', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}
/**
 * 冻结
 * @param params 
 * @returns 
 */
export async function userFrozen(params: { id: string }): Promise<any> {
    return request('/Admin/User/Frozen', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}
