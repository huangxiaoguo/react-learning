import React, { useRef, FC } from 'react';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import { PageContainer, WaterMark } from '@ant-design/pro-layout';
import { Card, Space } from 'antd';
import { connect, Dispatch, AccountListState } from 'umi';
import type { ListParamsType } from '@/services/data'
import { SingleUserListType, UserAddType } from './data'
import ProTable from '@ant-design/pro-table'
import './index.less'
import AddAccountModal from './components/addaccountmodal'
/**
 * 声明下方 props 类型
 */
type accountListPageProps = {
  listData: AccountListState,
  dispatch: Dispatch
}

const AccountListPage: FC<accountListPageProps> = (props) => {

  //获取从model中来的数据    
  const { listData, dispatch } = props
  //配置ProTable
  const ref = useRef<ActionType>();

  /**
    * ProTable的网络请求 request
    */
  const requestHandler = async (params: ListParamsType) => {
    await dispatch({
      type: 'accountListData/getRemoteUserList',
      payload: params = { ...params, perpage: params.pageSize, page: params.current }
    })
    return {}
  }

  //提交数据
  const onCreateFinish = async (values: UserAddType) => {
    await dispatch({
      type: 'accountListData/postRemoteUserAdd',
      payload: { ...values }
    })
    ref.current?.reload();
    return true
  }
  /**
    * 启用或停用
    * @param id 
    */
  const onAccountTypeClick = async (id: number) => {
    await dispatch({
      type: 'accountListData/postRemoteUserFrozen',
      payload: { id: id }
    })
    ref.current?.reload();
  }
  
  const columns: ProColumns<SingleUserListType>[] = [
    {
      title: 'ID',
      dataIndex: 'id',
      width: 128,
      search: false,
      align: 'left'
    },
    {
      title: '角色名称',
      dataIndex: 'role_name',
      ellipsis: true,
      search: false,
      align: 'left'
    },

    {
      title: '状态',
      dataIndex: 'is_enable',
      search: false,
      align: 'left',
      render: (text) => <span>{text == 1 ? `启用` : `停用`}</span>,
    },
    {
      title: '账号名称',
      dataIndex: 'account',
      hideInTable: true,
    },
    {
      title: '联系人姓名',
      dataIndex: 'contact_name',
      hideInTable: true,
    },
    {
      title: '创建时间',
      dataIndex: 'ctime_str',
      search: false,
      align: 'left'
    },
    {
      title: '操作',
      align: 'center',
      render: (text, record) => (
        <Space size="middle">
          <AddAccountModal
            userId={record.id}
            itemInfo={listData.info}
            roleList={listData.role_list}
            dispatch={dispatch}
            showType={1}
            onCreateFinish={onCreateFinish} />
          {
            record.is_enable == 0 ? <a className="start-using" onClick={() => onAccountTypeClick(record.id)}>启用</a> :
              <a className="stop-using" onClick={() => onAccountTypeClick(record.id)}>停用</a>
          }
        </Space>
      ),
    },
  ];
  return (
    <PageContainer>
      <Card>
        <WaterMark content="御码科技">
          <ProTable
            actionRef={ref}
            request={requestHandler}
            columns={columns}
            dataSource={listData.rows}
            rowKey="id"
            search={{
              labelWidth: 'auto',
            }}
            pagination={{
              showQuickJumper: true,
              pageSize: 10,
              total: listData.total
            }}
            form={{
              span: 6
            }}
            toolBarRender={() => [
              <AddAccountModal
                userId={0}
                itemInfo={listData.info}
                roleList={listData.role_list}
                dispatch={dispatch}
                showType={0}
                onCreateFinish={onCreateFinish} />
            ]}
          />
        </WaterMark>
      </Card>
    </PageContainer>
  )
}

const mapStateToProps = ({ accountListData }: { accountListData: AccountListState }) => {
  return {
    listData: accountListData,//这里的usersData就是model中的namespace
  }
}
export default connect(mapStateToProps)(AccountListPage)