import { Effect, Reducer } from 'umi';

//导入service远端数据请求
import { getUserList, getUserInfo, userAdd, userFrozen } from './service'

import { SingleUserListType, AccountInfoType, AccountRoleListType } from './data'
import { message } from 'antd';


export type AccountListState = {
    rows: SingleUserListType[];
    total: number;
    info: AccountInfoType;
    role_list: AccountRoleListType[]
}

interface AccountListModelType {
    namespace: string
    state: AccountListState;//封装后台返回的数据
    effects: {
        getRemoteUserList: Effect;
        getRemoteUserInfoData: Effect;
        postRemoteUserAdd: Effect;
        postRemoteUserFrozen: Effect;
    };
    reducers: {
        getUserList: Reducer<AccountListState>,
        getUserInfoData: Reducer<AccountListState>,
    };
}

const AccountListModel: AccountListModelType = {
    namespace: 'accountListData',
    state: {
        rows: [],
        total: 0,
        info: {},
        role_list: []
    },
    effects: {
        *getRemoteUserList({ payload }, { call, put }) {
            //从service中获取数据（service主要用于接口请求）
            const response = yield call(getUserList, { ...payload })
            if (response && response instanceof Object) {
                yield put({
                    type: 'getUserList',//这个是将数据给reducers中哪个方法
                    payload: response.data  //注意这里传递一个数组会出问题，原因待定
                })
            }
        },
        //獲取账户信息
        *getRemoteUserInfoData({ payload }, { call, put }) {
            const response = yield call(getUserInfo, { ...payload })
            if (response && response instanceof Object) {
                let { role_list } = response.data
                role_list.forEach((element: AccountRoleListType) => {
                    element.value = element.id,
                        element.label = element.role_name,
                        element.disabled = element.is_enable == 0
                })
                response.data.role_list = role_list
                response.data.info = { ...response.data.info, role_id: (response.data.info.role_id == 0 ? '' : response.data.info.role_id) }
                yield put({
                    type: 'getUserInfoData',
                    payload: response.data
                })
            }
        },
        //用户添加
        *postRemoteUserAdd({ payload }, { call, put }) {
            const response = yield call(userAdd, { ...payload })
            if (response && response instanceof Object) {
                message.success(response.message)
            }
        },
        //冻结
        *postRemoteUserFrozen({ payload }, { call, put }) {
            const response = yield call(userFrozen, { ...payload })
            if (response && response instanceof Object) {
                message.success(response.message)
            }
        }
    },
    //同步
    reducers: {
        getUserList(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
        getUserInfoData(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        }
    },
}

export default AccountListModel;