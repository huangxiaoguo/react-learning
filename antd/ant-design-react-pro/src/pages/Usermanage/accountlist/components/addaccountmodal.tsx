import React, { useEffect, FC } from 'react'
import { Button, Form } from 'antd';
import {
    ModalForm,
    ProFormText,
    ProFormSelect,
} from '@ant-design/pro-form';
import { PlusOutlined } from '@ant-design/icons';
import type { UserAddType } from '../data'
import { Dispatch } from 'umi';

type AddAccountModalProps = {
    showType: number
    onCreateFinish: (values: UserAddType) => void
    dispatch: Dispatch
    userId: number
    itemInfo: any
    roleList: any
}

const AddAccountModal: FC<AddAccountModalProps> = (props) => {

    const { showType, onCreateFinish, dispatch, userId, itemInfo, roleList } = props

    useEffect(() => {
        form.setFieldsValue({
            ...itemInfo,
        })
    }, [itemInfo])

    const [form] = Form.useForm();
    //ModalForm状态改变
    const onVisibleChange = async (value: boolean) => {
        if (value) {
            await dispatch({
                type: 'accountListData/getRemoteUserInfoData',
                payload: { id: userId }
            })
        }
    }

    return (
        <ModalForm<{
            account: string;
            account_password: string;
            contact_name: string;
            contact_mobile: number;
            role_id: number;
            user_id: number
        }>
            width="35%"
            form={form}
            title={showType === 0 ? `添加账户` : `编辑账户`}
            trigger={
                showType === 0 ? (
                    <Button type="primary">
                        <PlusOutlined />
                        添加账户
                    </Button>
                ) : (
                    <a>编辑</a>
                )
            }
            onFinish={async (values) => {
                return onCreateFinish(values = { ...values, user_id: userId });
            }}
            onVisibleChange={onVisibleChange}
        >
            <ProFormText name="account"
                label="账户名称："
                placeholder="请填写账户名称"
                rules={[{ required: true, message: '您还没填写账户名称' }]}
            />
            <ProFormText name="account_password"
                label="账户密码："
                placeholder={userId == 0 ? `请填写账户密码` : `修改则填写新密码，不修改则不填`}
                rules={[{
                    required: true, validator: (rule, value = '', callback) => {
                        if (Object.keys(value).length == 0) {
                            if (userId) {
                                return Promise.resolve()
                            } else {
                                return Promise.reject('您还没填写账户密码');
                            }
                        } else if (!new RegExp(/^(?!^(\d+|[a-zA-Z]+|[~!@#$%^&*?]+)$)^[\w~!@#$%^&*?]{6,20}$/).test(value)) {
                            return Promise.reject('必须含有数字、字母、特殊符号三项中间的两项,6到20位')
                        } else {
                            return Promise.resolve()
                        }
                    }
                }]}
            />
            <ProFormText name="contact_name"
                label="姓名："
                placeholder="请填写姓名"
                rules={[{ required: true, message: '您还没填写姓名' }]}
            />
            <ProFormText name="contact_mobile"
                label="手机号："
                placeholder="请填写手机号"
                rules={[{ required: true, message: '您还没填写手机号' },
                {
                    pattern: /^((13[0-9])|(14[5,7])|(15[0-3,5-9])|166|(17[0,3,5-8])|(18[0-9])|19[1,8,9]|(147))([0-9]{8})+$/,
                    message: '手机号格式不正确',
                },]}
            />
            <ProFormSelect
                options={roleList}
                width="md"
                name="role_id"
                label="绑定角色："
                placeholder="请选择绑定角色"
                rules={[{ required: true, message: '您还没选择绑定角色' }]}
            />
        </ModalForm>
    );
};

export default AddAccountModal;