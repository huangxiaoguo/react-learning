
/**
 * 封装后台返回的列表数据
 */
 export type SingleBannerListType = {
    id: number,
    banner?: string,
    location_id?: number,
    ad_type?: number,
    sort?: number,
    ad_content?: string,
    ctime?: string,
    uptime?: string,
    location_str?: string,
    ad_type_str?: string
}

/**
 * 添加banner
 */
 export type EditBannerType = {
    location_id: number,
    banner_id?: string,
    banner: number,
    ad_type: number,
    sort: number,
    content: string,
}