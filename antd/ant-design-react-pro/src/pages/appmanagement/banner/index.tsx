import React, { useState, useRef, FC } from 'react';
import { PageContainer, WaterMark } from '@ant-design/pro-layout';
import { Card, Space, Button, Input, message } from 'antd';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table'
import { PlusOutlined } from '@ant-design/icons';
import { SingleBannerListType } from './data'
import type { ListParamsType } from '@/services/data'
import { getBannerList, bannerSort, DelBannerDetail } from './service';
import './index.less'
import { Link } from 'umi';

const BannerListPage: FC<any> = () => {
  const [sortValue, setSortValue] = useState<string>('0');
  //配置ProTable
  const ref = useRef<ActionType>();
  /**
   * ProTable的网络请求 request
   */
  const requestHandler = async (params: ListParamsType) => {
    const roleList = await getBannerList(params = { ...params, perpage: params.pageSize, page: params.current });
    if (roleList && roleList instanceof Object) {
      return {
        data: [...roleList.data.rows],
        success: true,
        total: roleList.data?.total
      }
    }
    return {
      success: false,
    }
  }

  /**
   *  banner列表单排序
   * @param id 
   */
  const onSortBlur = async (id: number) => {
    const response = await bannerSort({ id: id, sort: sortValue });
    if (response && response instanceof Object) {
      message.success(response.message)
      ref.current?.reload()
    }
  }
  const onSortChange = (e: any) => {
    const { value } = e.target;
    setSortValue(value)
  }
  /**
   * 
   * @param id 删除
   */
  const onDetaleBannerClick = async (id: number) => {
    console.log(id)
    const response = await DelBannerDetail({ id: id });
    if (response && response instanceof Object) {
      message.success(response.message)
      ref.current?.reload()
    }
  }

  const columns: ProColumns<SingleBannerListType>[] = [
    {
      title: '排序',
      dataIndex: 'sort',
      width: 128,
      search: false,
      align: 'left',
      render: (text, record) => <div className="input-width"><Input onBlur={() => onSortBlur(record.id)} defaultValue={record.sort}
        onChange={onSortChange}
      /></div>,
    },
    {
      title: '广告位置',
      dataIndex: 'location_str',
      ellipsis: true,
      align: 'left',
      hideInSearch: true,
    },
    {
      title: '广告位置',
      dataIndex: 'location_id',
      ellipsis: true,
      align: 'left',
      filters: true,
      onFilter: true,
      hideInTable: true,
      valueType: 'select',
      valueEnum: {
        1: { text: '收听页面', status: 'Default' },
      },
    },
    {
      title: '内容类型',
      dataIndex: 'ad_type_str',
      align: 'left',
      hideInSearch: true,
    },
    {
      title: '内容类型',
      dataIndex: 'ad_type',
      align: 'left',
      filters: true,
      onFilter: true,
      hideInTable: true,
      valueType: 'select',
      valueEnum: {
        1: { text: '图文', status: 'Default' },
        2: { text: '链接', status: 'Default' },
        3: { text: '无内容', status: 'Default' },
      },
    },
    {
      title: '操作',
      align: 'center',
      render: (text, record) => (
        <Space size="middle">
          <Link to={{ pathname: '/appmanagement/banner/addbanner', state: { id: record.id } }}>编辑</Link>
          <a className="delete-item" onClick={() => onDetaleBannerClick(record.id)}>删除</a>
        </Space>
      ),
    },
  ];

  return (
    <PageContainer>
      <Card>
        <WaterMark content="御码科技">
          <ProTable
            request={requestHandler}
            columns={columns}
            rowKey="id"
            actionRef={ref}
            search={{
              labelWidth: 'auto',
            }}
            pagination={{
              showQuickJumper: true,
              pageSize: 10,
            }}
            form={{
              span: 6
            }}
            toolBarRender={() => [
              <Link to={{ pathname: '/appmanagement/banner/addbanner', state: { id: 0 } }}>
                <Button key="button" icon={<PlusOutlined />} type="primary">
                  添加广告
                </Button>
              </Link>
            ]}
          />
        </WaterMark>
      </Card>
    </PageContainer>
  )
}
export default BannerListPage