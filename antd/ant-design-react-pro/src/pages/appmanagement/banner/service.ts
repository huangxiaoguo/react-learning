import request from '@/utils/request';
import type { ListParamsType } from '@/services/data'
import type { EditBannerType } from './data'

/**
 * 列表数据
 * @param params 
 * @returns 
 */
export async function getBannerList(params: ListParamsType): Promise<any> {
    return request('/Admin/App/Banner/List', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}

/**
 * banner列表单排序
 * @param params 
 * @returns 
 */
export async function bannerSort(params: { id: number, sort: string }): Promise<any> {
    return request('/Admin/App/Banner/Sort', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}
/**
 * 获取banner设置详情
 * @param params 
 * @returns 
 */
export async function getBannerDetail(params: { id: number }): Promise<any> {
    return request('/Admin/App/Banner/Add', {
        method: 'GET',
        params: { ...params },
        requestType: 'form'
    });
}
/**
 * 添加banner
 * @param params 
 * @returns 
 */
export async function editBannerDetail(params: EditBannerType): Promise<any> {
    return request('/Admin/App/Banner/Add', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}
/**
 * 删除banner
 * @param params 
 * @returns 
 */
export async function DelBannerDetail(params: { id: number }): Promise<any> {
    return request('/Admin/App/Banner/Del', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}
/**
 * 图片上传参数
 */
export type ImgParamsType = {
    img_type: string;
    img_byte: string;
};
/**
 * 上传图片
 * @param params 
 * @returns 
 */
export async function getUpPic(params: ImgParamsType): Promise<any> {
    return request('/UpPic', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}
