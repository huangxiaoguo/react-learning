import React from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Card } from 'antd';
import './Welcome.less';

import welcome from '@/assets/welcome.png'

export default (): React.ReactNode => {
  return (
    <PageContainer>
      <Card>
        <div className="welcome">
          <img src={welcome} alt="欢迎" />
        </div>
      </Card>
    </PageContainer>
  );
};
