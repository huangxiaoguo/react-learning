import {
  LockOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Tabs } from 'antd';
import React from 'react';
import ProForm, { ProFormText } from '@ant-design/pro-form';
import { connect } from 'umi';
import type { Dispatch } from 'umi';
import type { LoginParamsType } from '@/services/login';
import type { ConnectState } from '@/models/connect';
import styles from './index.less';
export type LoginProps = {
  dispatch: Dispatch;
  submitting?: boolean;
};


const Login: React.FC<LoginProps> = (props) => {
  const { submitting } = props;

  const handleSubmit = (values: LoginParamsType) => {
    const { dispatch } = props;
    dispatch({
      type: 'login/login',
      payload: { ...values },
    });
  };

  return (
    <div className={styles.main}>
      <ProForm
        submitter={{
          render: (_, dom) => dom.pop(),
          submitButtonProps: {
            loading: submitting,
            size: 'large',
            style: {
              width: '100%',
            },
          },
        }}
        onFinish={(values) => {
          handleSubmit(values as LoginParamsType);
          return Promise.resolve();
        }}
      >
        <Tabs activeKey='account' >
          <Tabs.TabPane
            key="account"
            tab='账户密码登录'
          />
        </Tabs>
        <ProFormText
          name="user_name"
          fieldProps={{
            size: 'large',
            prefix: <UserOutlined className={styles.prefixIcon} />,
          }}
          placeholder='请填写用户名'
          rules={[
            {
              required: true,
              message: '用户名是必填项！',
            },
          ]}
        />
        <ProFormText.Password
          name="password"
          fieldProps={{
            size: 'large',
            prefix: <LockOutlined className={styles.prefixIcon} />,
          }}
          placeholder='请填写密码'
          rules={[
            {
              required: true,
              message: '密码是必填项！',
            },
          ]}
        />
      </ProForm>
    </div >
  );
};

export default connect(({loading }: ConnectState) => ({
  submitting: loading.effects['login/login'],
}))(Login);
