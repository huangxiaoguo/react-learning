import React, { useState, useEffect } from 'react';
import { Button } from 'antd';
import {
    ModalForm,
    ProFormText,
    ProFormSelect,
} from '@ant-design/pro-form';
import { PlusOutlined } from '@ant-design/icons';
import type { CreateCodeType } from '../data'

type CreateCodeModalProps = {
    mealOptions: object,
    onCreateFinish: (values: CreateCodeType) => void
    onVisibleChange: (values: boolean) => void
}

export default (props: CreateCodeModalProps) => {
    const [mealOptionsList, setMealOptionsList] = useState<any>([]);

    const { mealOptions, onCreateFinish, onVisibleChange } = props
    useEffect(() => {
        if (mealOptions && mealOptions instanceof Object) {
            let arrs: Array<object> = []
            for (let [key, value] of Object.entries(mealOptions)) {
                const obj = {
                    value: key,
                    label: value.text,
                }
                arrs = [...arrs, obj]
            }
            setMealOptionsList(arrs)
        }
    }, [mealOptions])
    return (
        <ModalForm<{
            code_num: string;
            combo_id: string;
        }>
            width="35%"
            title="创建邀请码"
            trigger={
                <Button type="primary">
                    <PlusOutlined />
                    创建邀请码
                </Button>
            }
            onFinish={async (values) => {
                return onCreateFinish(values);
            }}
            onVisibleChange={onVisibleChange}
        >
            <ProFormText name="code_num"
                label="激活码数量："
                placeholder="请输入激活码生成数量"
                rules={[{ required: true, message: '您还没输入激活码生成数量' }]}
            />
            <ProFormSelect
                options={mealOptionsList}
                width="md"
                name="combo_id"
                label="套餐："
                placeholder="请选择套餐"
                rules={[{ required: true, message: '您还没选择套餐' }]}
            />
        </ModalForm>
    );
};