
/**
 * 封装后台返回的列表数据
 */
export type SingleVipCodeListType = {
    active_mid?: number,
    active_status?: number,
    active_time?: number,
    active_time_str?: string,
    batch_id?: number,
    card_no?: string,
    code_key?: string,
    combo_id?: number,
    combo_name?: string,
    combo_price?: number,
    combo_price_str?: string,
    ctime?: number,
    ctime_str?: string,
    duration?: number,
    duration_day?: string,
    id: number,
    mobile?: string,
    uptime?: number
}

/**
 * 创建激活码生成任务 :传入类型
 */
export type CreateCodeType = {
    code_num: string;
    combo_id: string;
};


/**
 * 根据列表筛选条件导出激活码
 */
export type ExpotCodeType = {
    card_no?: string;
    mobile?: string;
    start_time?: string;
    end_time?: string;
    combo_id?: string;
    ctime_str?: Array | undefined;
};