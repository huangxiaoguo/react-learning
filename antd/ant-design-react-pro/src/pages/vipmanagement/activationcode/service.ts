import request from '@/utils/request';
import type { ListParamsType } from '@/services/data'
import type { CreateCodeType, ExpotCodeType } from './data'
/**
 * 列表数据
 * @param params 
 * @returns 
 */
export async function getVipCodeList(params: ListParamsType): Promise<any> {
    return request('/Admin/Vip/Code/List', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}
/**
 * 获取——选择套餐
 * @param params 
 * @returns 
 */
export async function getSelectVipCodeList(): Promise<any> {
    return request('/Admin/Vip/Code/List');
}



/**
 * 创建激活码生成任务
 * @param params 
 * @returns 
 */
export async function createCode(params: CreateCodeType): Promise<any> {
    return request('/Admin/Vip/Code/Create', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}

/**
 * 根据列表筛选条件导出激活码
 * @param params 
 * @returns 
 */
export async function expotCode(params: ExpotCodeType): Promise<any> {
    return request('/Admin/Vip/Code/Export', {
        method: 'POST',
        timeout: 60000,
        data: { ...params },
        requestType: 'form'
    });
}