import request from '@/utils/request';
import type { ListParamsType } from '@/services/data'

/**
 * 列表数据
 * @param params 
 * @returns 
 */
export async function getCodeCreateList(params: ListParamsType): Promise<any> {
    return request('/Admin/Vip/Code/CreateList', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}
