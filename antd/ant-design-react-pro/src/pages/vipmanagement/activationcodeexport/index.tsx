import React, { FC } from 'react';
import { PageContainer, WaterMark } from '@ant-design/pro-layout';
import { Card, Space } from 'antd';
import type { ProColumns } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table'
import { SingleCodeExportType } from './data'
import type { ListParamsType } from '@/services/data'
import { getCodeExportList } from './service';
import './index.less'

const CodeCreateListPage: FC<any> = () => {

  /**
   * ProTable的网络请求 request
   */
  const requestHandler = async (params: ListParamsType) => {
    const roleList = await getCodeExportList(params = { ...params, perpage: params.pageSize, page: params.current });
    if (roleList && roleList instanceof Object) {
      return {
        data: roleList.data.rows,
        success: true,
        total: roleList.data?.total
      }
    }
    return {
      success: false,
    }
  }
  const columns: ProColumns<SingleCodeExportType>[] = [
    {
      title: '创建数量',
      dataIndex: 'code_num',
      width: 168,
      search: false,
      align: 'left'
    },
    {
      title: '创建时间',
      dataIndex: 'ctime_str',
      ellipsis: true,
      search: false,
      align: 'left'
    },
    {
      title: '完成时间',
      dataIndex: 'success_time_str',
      ellipsis: true,
      search: false,
      align: 'left'
    },
    {
      title: '状态',
      dataIndex: 'status',
      search: false,
      align: 'left',
      render: (text) => <span className={text === 1 ? `activated` : `no-use`}>{text === 1 ? `已完成` : `未完成`}</span>,
    },
    {
      title: '操作',
      align: 'center',
      width: 168,
      render: (text, record) => (
        <Space size="middle">
          <a href={record.file_path} download="11"><b>点击下载</b></a>
        </Space>
      ),
    },
  ];

  return (
    <PageContainer>
      <Card>
        <WaterMark content="御码科技">
          <ProTable
            request={requestHandler}
            columns={columns}
            rowKey="id"
            search={false}
            pagination={{
              showQuickJumper: true,
              pageSize: 10,
            }}
            form={{
              span: 6
            }}
          />
        </WaterMark>
      </Card>
    </PageContainer>
  )
}
export default CodeCreateListPage