import request from '@/utils/request';
import type { ListParamsType } from '@/services/data'

/**
 * 列表数据
 * @param params 
 * @returns 
 */
export async function getCodeExportList(params: ListParamsType): Promise<any> {
    return request('/Admin/Vip/Code/ExportList', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}
