
/**
 * 封装后台返回的列表数据
 */
export type SingleCodeExportType = {
    id: number,
    user_id?: number,
    code_num?: number,
    status?: number,
    success_time?: number,
    ctime?: number,
    uptime?: number,
    file_path?: string,
    ctime_str?: string,
    success_time_str?: string
}