
/**
 * ProTable 搜索类型
 */
export type ListParamsType = {
    perpage: number;
    page: number;
    current: number;
    pageSize: number;
};