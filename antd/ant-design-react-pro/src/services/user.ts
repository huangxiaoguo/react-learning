import request from '@/utils/request';

/**
 * 
 * @returns 首页列表权限
 */
export async function queryCurrent():Promise<any>{
  return request('/user/info2.json', {
    prefix:'/mock',
    method: 'GET',
  });
}