import request from '@/utils/request';

export type LoginParamsType = {
  user_name: string;
  password: string;
};

/**
 * 
 * @param params 登录接口
 * @returns 
 */
export async function fakeAccountLogin(params: LoginParamsType):Promise<any> {
  return request('/Admin/Login', {
    method: 'POST',
    params
  });
}
