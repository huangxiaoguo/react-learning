import { message } from 'antd';
import { RequestOptionsInit } from 'umi-request';
import { getToken } from '@/utils/auth'
import { getPageQuery } from '@/utils/utils';
import { history } from 'umi';
import { stringify } from 'querystring';
import { removeToken } from '@/utils/auth'

const codeMessage: { [status: string]: string } = {
    200: '服务器成功返回请求的数据。',
    201: '新建或修改数据成功。',
    202: '一个请求已经进入后台排队（异步任务）。',
    204: '删除数据成功。',
    400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
    401: '用户没有权限（令牌、用户名、密码错误）。',
    403: '用户得到授权，但是访问是被禁止的。',
    404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
    406: '请求的格式不可得。',
    410: '请求的资源被永久删除，且不会再得到的。',
    422: '当创建一个对象时，发生一个验证错误。',
    500: '服务器发生错误，请检查服务器。',
    502: '网关错误。',
    503: '服务不可用，服务器暂时过载或维护。',
    504: '网关超时。',
};

/** 异常处理程序 */
export const errorHandler = (error: { response: Response }): Response => {
    const { response } = error;
    console.log(response)
    if (response && response.status) {
        const errorText = codeMessage[response.status] || response.statusText;
        message.error(errorText);
    } else if (!response) {
        message.error('您的网络发生异常，无法连接服务器');
    }
    return response;
};


/**
 * request拦截器, 改变url 或 options.
 * @returns 
 */
export const requestOpt = (url: string, options: RequestOptionsInit): object => {
    console.log("url==>", url)
    if (options.data) {
        console.log("options.data==>", JSON.stringify(options.data))
    } else if (options.params && Object.keys(options.params).length > 0) {
        console.log("options.params==>", options.params)
    }
    if (process.env.NODE_ENV === 'development' && !options.prefix) {
        url = '/api' + url
    } else {
        if (url !== '/mock/user/info2.json') {
            url = 'http://guoluren.xicp.net:9003' + url
        }
    }
    const headers = {
        'token': getToken(),
    };
    return {
        url: url,
        options: { ...options, headers },
    };
}

export const reLogin = () => {
    const { redirect } = getPageQuery();
    // Note: There may be security issues, please note
    if (window.location.pathname !== '/user/login' && !redirect) {
        removeToken()
        history.replace({
            pathname: '/user/login',
            search: stringify({
                redirect: window.location.href,
            }),
        });
    }
}
