/** Request 网络请求工具 更详细的 api 文档: https://github.com/umijs/umi-request */
import { message } from 'antd';
import { extend } from 'umi-request';
import { errorHandler, requestOpt, reLogin } from './requestopt';


/** 配置request请求时的默认参数 */
const request = extend({
  errorHandler, // 默认错误处理
  credentials: 'include', // 默认请求是否带上cookie
});

// request拦截器, 改变url 或 options.
request.interceptors.request.use((url, options) => {
  return requestOpt(url, options)
});

// response拦截器, 处理response
request.interceptors.response.use(async response => {
  const data = await response.clone().json();
  console.log(data)
  if (data.code !== 200) {
    message.error(data.message);
    if (data.code === 300) {
      reLogin()
    }
    return data.message
  } else {
    return response;
  }
})
export default request
