import React, { useState, FC } from 'react'
import { WaterMark } from '@ant-design/pro-layout';
import { Editor } from '@tinymce/tinymce-react';
import '../../../public/tinymce/themes/silver/theme.min.js'//编辑器主题
import '../../../public/tinymce/icons/default/icons.min.js';//图标
import '../../../public/tinymce/plugins/advlist/plugin.min.js'  //高级列表
import '../../../public/tinymce/plugins/autolink/plugin.min.js'  //自动链接
import '../../../public/tinymce/plugins/link/plugin.min.js'  //超链接
import '../../../public/tinymce/plugins/lists/plugin.min.js' //列表插件
import '../../../public/tinymce/plugins/charmap/plugin.min.js'  //特殊字符
import '../../../public/tinymce/plugins/media/plugin.min.js' //插入编辑媒体
import '../../../public/tinymce/plugins/wordcount/plugin.min.js'// 字数统计
import '../../../public/tinymce/skins/ui/oxide/skin.min.css'
import '../../../public/tinymce/skins/ui/oxide/content.inline.min.css'
import '../../../public/tinymce/langs/zh_CN';
type UploadImageType = {
    height?: string,
    editorHtml?: string,
    handleImageUpload: (blobInfo: any, success: Function, failure: Function) => void,
    handleEditorTextChange: (text: string) => void
}
const UploadImage: FC<UploadImageType> = (props) => {
    const { height = '500', editorHtml, handleImageUpload, handleEditorTextChange } = props
    const handleEditorChange = (text: string) => {
        handleEditorTextChange(text)
    }

    return (
        <WaterMark content="御码科技" className="upload-image">
            <Editor
                inline={false}  // false：经典编辑模式；true:行内编辑模式
                initialValue={editorHtml}
                init={{
                    height: height,
                    language_url: 'tinymce/langs/zh_CN.js',
                    language: 'zh_CN',
                    // skin_url: 'tinymce/skins/ui/oxide',
                    content_style: '* { padding:0; margin:0; } img {max-width:100% !important }',
                    lineheight_val:
                        '1 1.1 1.2 1.3 1.35 1.4 1.5 1.55 1.6 1.75 1.8 1.9 1.95 2 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 3 3.1 3.2 3.3 3.4 4 5',
                    fontsize_formats: '8pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 24pt 36pt',
                    font_formats:
                        "微软雅黑='微软雅黑';宋体='宋体';黑体='黑体';仿宋='仿宋';楷体='楷体';隶书='隶书';幼圆='幼圆';Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings",
                    plugin_preview_width: 375, // 预览宽度
                    plugin_preview_height: 668,
                    plugins: 'link lists image code table wordcount media fullscreen preview paste',
                    powerpaste_word_import: 'merge',
                    toolbar:
                        'fontselect | bold italic underline strikethrough | link unlink image | undo redo | fontsizeselect | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent blockquote | code | removeformat',
                    paste_data_images: true,
                    statusbar: true, // 底部的状态栏
                    menubar: true, // 最上方的菜单
                    branding: false, // 水印“Powered by TinyMCE”
                    convert_urls: false,//去除URL转换
                    images_upload_handler: (blobInfo, success, failure) => {
                        handleImageUpload(blobInfo, success, failure)
                    },
                }}
                onEditorChange={handleEditorChange}
            />
        </WaterMark>
    );
}

export default UploadImage;