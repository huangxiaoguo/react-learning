import request from '@/utils/request';
/**
 * 图片上传参数
 */
 export type ImgParamsType = {
    img_type: string;
    img_byte: string;
};
/**
 * 上传图片
 * @param params 
 * @returns 
 */
 export async function getUpPic(params: ImgParamsType): Promise<any> {
    return request('/UpPic', {
        method: 'POST',
        data: { ...params },
        requestType: 'form'
    });
}
