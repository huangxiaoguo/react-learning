import { stringify } from 'querystring';
import type { Effect } from 'umi';
import { history } from 'umi';

import { fakeAccountLogin } from '@/services/login';
import { getPageQuery } from '@/utils/utils';
import { message } from 'antd';

import { setToken, removeToken } from '@/utils/auth'

export type LoginModelType = {
  namespace: string;
  state: object;
  effects: {
    login: Effect;
    logout: Effect;
  };
};

const Model: LoginModelType = {
  namespace: 'login',

  state: {
  },

  effects: {
    *login({ payload }, { call, put }) {
      const result = yield call(fakeAccountLogin, payload);
      if (result && result instanceof Object) {
        const urlParams = new URL(window.location.href);
        const params = getPageQuery();
        message.success('🎉 🎉 🎉  登录成功！');
        let { redirect } = params as { redirect: string };
        if (redirect) {
          const redirectUrlParams = new URL(redirect);
          if (redirectUrlParams.origin === urlParams.origin) {
            redirect = redirect.substr(urlParams.origin.length);
            if (redirect.match(/^\/.*#/)) {
              redirect = redirect.substr(redirect.indexOf('#') + 1);
            }
          } else {
            window.location.href = '/';
            return;
          }
        }
        setToken(result.data.token)
        history.replace(redirect || '/');
      }
    },

    *logout() {
      const { redirect } = getPageQuery();
      // Note: There may be security issues, please note
      if (window.location.pathname !== '/user/login' && !redirect) {
        removeToken()
        history.replace({
          pathname: '/user/login',
          search: stringify({
            redirect: window.location.href,
          }),
        });
      }
    },
  },
};

export default Model;
