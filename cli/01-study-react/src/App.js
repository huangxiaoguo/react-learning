import React, { Component } from 'react';
import Cat from "./components/Cat";
import Dog from "./components/Dog";

class App extends Component {

    render() {
        return (
            <div>
                <Cat />
                <Dog />
            </div>
        );
    }
}

export default App;