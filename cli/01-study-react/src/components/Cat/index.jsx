import React, { Component } from 'react'
import axios from 'axios'
import PubSub from 'pubsub-js'

export default class Cat extends Component {

    componentDidMount() {
        axios.get('/api1/user/info')
            .then(function (response) {
                console.log(response);
                PubSub.publish('recever', { isLoading: false, users: response.data })
            })
            .catch(function (error) {
                console.log(error);
                PubSub.publish('recever', { isLoading: false, err: error.message })
            });
    }

    render() {
        return (
            <div>
                我是Cat
            </div>
        )
    }
}
