import React, { Component } from 'react'
import PubSub from 'pubsub-js'
export default class Dog extends Component {

    componentDidMount() {
        this.token = PubSub.subscribe('recever', (_, stateObj) => {
            console.log('我接收到了数据', stateObj)
        })
    }
    componentWillUnmount() {
        PubSub.unsubscribe(this.token)
    }
    render() {
        return (
            <div>
                我是Dog
            </div>
        )
    }
}
