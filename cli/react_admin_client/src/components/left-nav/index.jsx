import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom';
import { Menu } from 'antd';

import './index.less'
import logo from '../../assets/images/logo.png';
import menuList from '../../config/menuConfig'

const { SubMenu } = Menu;

class LeftNav extends Component {
    /*
       一、 第一种方案
        根据menu的数据数组生成对应的标签数组
        使用map() + 递归调用
    */
    getMenuNodesMap = (menuList) => {
        // 得到当前请求的路由路径
        const path = this.props.location.pathname
        return menuList.map(item => {
            if (!item.children) {
                return (
                    <Menu.Item key={item.key} icon={item.icon}>
                        <Link to={item.key}>
                            <span>{item.title}</span>
                        </Link>
                    </Menu.Item>
                )
            } else {
                // 查找一个与当前请求路径匹配的子Item
                const cItem = item.children.find(cItem => path.indexOf(cItem.key) === 0)
                // 如果存在, 说明当前item的子列表需要打开
                if (cItem) {
                    this.openKey = item.key
                }
                return (
                    <SubMenu
                        key={item.key}
                        icon={item.icon}
                        title={item.title}
                    >
                        {this.getMenuNodesMap(item.children)}
                    </SubMenu>
                )
            }

        })
    }

    /*
    二、第二种方法
    根据menu的数据数组生成对应的标签数组
    使用reduce() + 递归调用
    */
    getMenuNodesReduce = (menuList) => {
        // 得到当前请求的路由路径
        const path = this.props.location.pathname
        return menuList.reduce((pre, item) => {

            // 如果当前用户有item对应的权限, 才需要显示对应的菜单项
            if (this.hasAuth(item)) {
                // 向pre添加<Menu.Item>
                if (!item.children) {
                    pre.push((
                        <Menu.Item key={item.key} icon={item.icon}>
                            <Link to={item.key}>
                                {item.title}
                            </Link>
                        </Menu.Item>
                    ))
                } else {

                    // 查找一个与当前请求路径匹配的子Item
                    const cItem = item.children.find(cItem => path.indexOf(cItem.key) === 0)
                    // 如果存在, 说明当前item的子列表需要打开
                    if (cItem) {
                        this.openKey = item.key
                    }

                    // 向pre添加<SubMenu>
                    pre.push((
                        <SubMenu
                            key={item.key}
                            icon={item.icon}
                            title={item.title}
                        >
                            {this.getMenuNodesReduce(item.children)}
                        </SubMenu>
                    ))
                }
            }

            return pre
        }, [])
    }
    /*
   在第一次render()之前执行一次
   为第一个render()准备数据(必须同步的)
    */
    UNSAFE_componentWillMount() {
        this.menuNodes = this.getMenuNodesMap(menuList)
        // this.menuNodes = this.getMenuNodesReduce(menuList)
    }
    render() {
        // 得到当前请求的路由路径
        const path = this.props.location.pathname
        console.log(path)
        // 得到需要打开菜单项的key
        const openKey = this.openKey
        return (
            <div className="left-nav">
                <Link to='/' className="left-nav-header">
                    <img src={logo} alt="logo" />
                    <div className="title"> 硅谷后台</div>
                </Link>
                <Menu
                    selectedKeys={[path]}
                    defaultOpenKeys={[openKey]}
                    mode="inline"
                    theme="dark"
                >
                    {
                        this.menuNodes
                    }
                </Menu>
            </div>
        )
    }
}

/*
withRouter高阶组件:
包装非路由组件, 返回一个新的组件
新的组件向非路由组件传递3个属性: history/location/match
 */
export default withRouter(LeftNav)