import React, { Component } from 'react'
import {
    Card,
    Table,
    Button,
    message,
    Modal
} from 'antd';
import {
    PlusOutlined,
    RightOutlined
} from '@ant-design/icons';
import LinkButton from '../../components/link-button'
import { reqCategorys } from '../../api'

export default class Category extends Component {
    state = {
        loading: false, // 是否正在获取数据中
        categorys: [], // 一级分类列表
        subCategorys: [], // 二级分类列表
        parentId: '0', // 当前需要显示的分类列表的父分类ID
        parentName: '', // 当前需要显示的分类列表的父分类名称
        showStatus: 0, // 标识添加/更新的确认框是否显示, 0: 都不显示, 1: 显示添加, 2: 显示更新
    }



    /*
    异步获取一级/二级分类列表显示
    parentId: 如果没有指定根据状态中的parentId请求, 如果指定了根据指定的请求
     */
    getCategorys = async (parentId) => {

        // 在发请求前, 显示loading
        this.setState({ loading: true })
        parentId = parentId || this.state.parentId
        // 发异步ajax请求, 获取数据
        const result = await reqCategorys(parentId)
        // 在请求完成后, 隐藏loading
        this.setState({ loading: false })

        // 取出分类数组(可能是一级也可能二级的)
        const categorys = result
        if (parentId === '0') {
            // 更新一级分类状态
            this.setState({
                categorys
            })
        } else {
            // 更新二级分类状态
            this.setState({
                subCategorys: categorys
            })
        }

    }
    /*
     显示指定一级分类对象的二子列表
      */
    showSubCategorys = (category) => {
        // 更新状态
        this.setState({
            parentId: category._id,
            parentName: category.name
        }, () => {
            // 获取二级分类列表显示
            this.getCategorys()
        })
    }

    /*
    初始化Table所有列的数组
    */
    initColumns = () => {
        this.columns = [
            { title: '分类名称', dataIndex: 'name', key: '_id', align: 'left' },
            {
                title: '操作',
                align: 'left',
                width: 300,
                render: (category) => (
                    <span>
                        <LinkButton onClick={() => this.showUpdate(category)}>修改分类</LinkButton>
                        {/*如何向事件回调函数传递参数: 先定义一个匿名函数, 在函数调用处理的函数并传入数据*/}
                        {this.state.parentId === '0' ? <LinkButton onClick={() => this.showSubCategorys(category)}>查看子分类</LinkButton> : null}
                    </span>
                ),
            },
        ]
    }
    /*
        为第一次render()准备数据
    */
    UNSAFE_componentWillMount() {
        this.initColumns()
    }
    /*
        执行异步任务: 发异步ajax请求
    */
    componentDidMount() {
        // 获取一级分类列表显示
        this.getCategorys()
    }

    render() {
        // 读取状态数据
        const { categorys, subCategorys, parentId, parentName, loading, showStatus } = this.state

        // card的左侧
        const title = parentId === '0' ? '一级分类列表' : (
            <span>
                <LinkButton icon={<RightOutlined />}>一级分类列表</LinkButton>
                <span>{parentName}</span>
            </span>
        )
        // Card的右侧
        const extra = (
            <Button type='primary' icon={<PlusOutlined />} >添加</Button>)
        return (
            <div>
                <Card title={title} extra={extra} style={{ width: '100%' }}>
                    <Table
                        bordered
                        loading={loading}
                        columns={this.columns}
                        dataSource={parentId === '0' ? categorys : subCategorys}
                    />
                </Card>
            </div>
        )
    }
}
