import React, { Component } from 'react'
import './login.less'
import logo from '../../assets/images/logo.png'
import { Redirect } from 'react-router-dom';

import { Form, Input, Button } from 'antd';

import { reqLogin } from "../../api/index";

import memoryUtils from "../../utils/memoryUtils";
import storageUtils from '../../utils/storageUtils'

const layout = {
    labelCol: { span: 4 },
    wrapperCol: { span: 20 },
};
const tailLayout = {
    wrapperCol: { offset: 4, span: 20 },
};

/**
 * 登录的路由组件
 */
export default class Login extends Component {
    state = {
        loading: false
    }
    onFinish = (values) => {
        const { username, password } = values
        this.setState({ loading: true }, async () => {
            try {
                const response = await reqLogin(username, password)
                const user = response
                memoryUtils.user = user
                storageUtils.saveUser(memoryUtils.user)
                this.setState({ loading: false })
                //跳转到管理页面（不需要再返回到登录）
                this.props.history.replace('/')

            } catch (error) {
                this.setState({ loading: false })
            }
        })
    };
    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    render() {
        const { user } = memoryUtils
        if (user && user._id) {
            return <Redirect to='/' />
        }
        const { loading } = this.state;
        return (
            <div className="login">
                <div className="login-header">
                    <div className="login-header-img"><img src={logo} alt="logo" /></div>
                    <div className="login-header-name">React项目：后台管理系统</div>
                </div>
                <div className="login-content">
                    <div className="content-title">用户登录</div>
                    <Form
                        className="login-form "
                        {...layout}
                        name="basic"
                        initialValues={{ username: 'admin', password: 'admin' }}//默认初始值
                        onFinish={this.onFinish}
                        onFinishFailed={this.onFinishFailed}
                    >
                        {/* {
                                pattern: /^((13[0-9])|(14[5,7])|(15[0-3,5-9])|166|(17[0,3,5-8])|(18[0-9])|19[1,8,9]|(147))([0-9]{8})+$/,
                                message: '手机号格式不正确',
                                trigger: 'blur',
                            }, */}
                        <Form.Item
                            label="账号"
                            name="username"
                            rules={[{ required: true, message: '请输入手机号', trigger: 'blur' },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        {/* {
                                pattern: /^(?!^(\d+|[a-zA-Z]+|[~!@#$%^&*?]+)$)^[\w~!@#$%^&*?]{6,20}$/,
                                message: '必须含有数字、字母、特殊符号三项中间的两项,6到20位',
                            }, */}
                        <Form.Item
                            label="密码"
                            name="password"
                            rules={[{ required: true, message: '请输入密码', trigger: 'blur' },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>

                        <Form.Item {...tailLayout}>
                            <Button type="primary" htmlType="submit" loading={loading} className="login-form-button">
                                登录
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        )
    }
}
